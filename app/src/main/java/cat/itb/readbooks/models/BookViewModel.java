package cat.itb.readbooks.models;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class BookViewModel extends ViewModel {

   public static List<Book> bookList = new ArrayList<>();

    public List<Book> getBooks() {
        return bookList;
    }

    public BookViewModel() {
        String[] status_value = {
                "Want to read",
                "Reading",
                "Read" };

        String[] titles = {
                "La comunidad del anillo",
                "Las dos torres",
                "El retorno del rey",
                "eL Hobbit" };

        String[] authors = {
                "J.R.R. Tolkien",
                "J.R.R. Tolkien",
                "J.R.R. Tolkien",
                "J.R.R. Tolkien"};

        for (int i = 0; i < titles.length; i++) {
            int status = (int) (Math.random() * (2+1));
            int rate;
            if (status != 2) {
                rate = 0;
            } else {
                rate = (int) (Math.random() * (5+1));
            }
            Book b = new Book(titles[i], authors[i], status_value[status], rate);
            bookList.add(b);
        }
    }
}