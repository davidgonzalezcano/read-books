package cat.itb.readbooks.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.readbooks.R;
import cat.itb.readbooks.fragments.BookListFragmentDirections;
import cat.itb.readbooks.models.Book;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder>{
    List<Book> books;
    public BookAdapter(List<Book> books) {
        this.books = books;
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {
        private TextView bookTitleValue;
        private TextView bookAuthorValue;
        private TextView bookStatusValue;
        private RatingBar myRating;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);

            bookTitleValue = itemView.findViewById(R.id.book_title_value);
            bookAuthorValue = itemView.findViewById(R.id.book_author_value);
            bookStatusValue = itemView.findViewById(R.id.book_status_value);
            myRating = itemView.findViewById(R.id.myRating);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    NavDirections navDir = BookListFragmentDirections
                            .actionBookListFragmentToBookFragment(books.get(getAdapterPosition()));

                    Navigation.findNavController(v).navigate(navDir);
                }
            });
        }

        public void bind(Book b) {
            bookTitleValue.setText(b.getTitle());
            bookAuthorValue.setText(b.getAuthor());
            bookStatusValue.setText(b.getStatus());

            if (b.getStatus().equals("Read")) {
                myRating.setRating(b.getRate());
            }
        }
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.book_item_list_fragment, parent, false);
        return new BookViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        Book b = books.get(position);
        holder.bind(b);
    }

    @Override
    public int getItemCount() {
        return books.size();
    }
}
