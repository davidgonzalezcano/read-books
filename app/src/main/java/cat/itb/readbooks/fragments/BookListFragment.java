package cat.itb.readbooks.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import cat.itb.readbooks.R;
import cat.itb.readbooks.adapters.BookAdapter;
import cat.itb.readbooks.models.BookViewModel;

public class BookListFragment extends Fragment {
    private BookViewModel bookViewModel;
    private FloatingActionButton floatingActionButton;
    private BookAdapter bookAdapter;
    private RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bookViewModel = new ViewModelProvider(requireActivity()).get(BookViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.book_list_fragment, container, false);

        recyclerView = v.findViewById(R.id.recyclerview_fragment);
        floatingActionButton = v.findViewById(R.id.floatingActionButton);
        bookAdapter = new BookAdapter(bookViewModel.getBooks());

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(bookAdapter);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(
                        R.id.action_bookListFragment_to_bookFragment);
            }
        });
        return v;
    }
}