package cat.itb.readbooks.fragments;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import cat.itb.readbooks.R;
import cat.itb.readbooks.models.Book;
import cat.itb.readbooks.models.BookViewModel;

public class BookFragment extends Fragment {
    private TextView addBook;
    private EditText addBookTitleValue;
    private EditText addBookAuthorValue;
    private Spinner addBookStatusValue;
    private Button addBtn, updateBtn;
    private Book book;
    private RatingBar ratingBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.book_fragment, container, false);

        addBook = v.findViewById(R.id.add_book);
        addBookTitleValue = v.findViewById(R.id.add_book_title_value);
        addBookAuthorValue = v.findViewById(R.id.add_book_author_value);
        addBookStatusValue = v.findViewById(R.id.add_book_status_value);
        ratingBar = v.findViewById(R.id.ratingBar);
        addBtn = v.findViewById(R.id.add_btn);
        updateBtn = v.findViewById(R.id.update_btn);

        ArrayAdapter<CharSequence> statusAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.status_values, android.R.layout.simple_spinner_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        addBookStatusValue.setAdapter(statusAdapter);


        if (getArguments() != null) {
            book = getArguments().getParcelable("book");
        }

        if (book != null) {
            addBook.setText(R.string.bookSettings);
            addBookTitleValue.setText(book.getTitle());
            addBookAuthorValue.setText(book.getAuthor());
            addBtn.setVisibility(View.INVISIBLE);
            updateBtn.setVisibility(View.VISIBLE);

            if (book.getStatus().equals("Want to read")) { addBookStatusValue.setSelection(0); }

            else if (book.getStatus().equals("Reading")) { addBookStatusValue.setSelection(1); }

            else {
                addBookStatusValue.setSelection(2);
                ratingBar.setRating(book.getRate());
            }
        }
        else  {
            book = new Book();
            addBtn.setVisibility(View.VISIBLE);
            updateBtn.setVisibility(View.INVISIBLE);
        }

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        if(ratingBar.getRating() >= 1 &&
                                (book.getStatus().equals("Want to read") ||
                                book.getStatus().equals("Reading"))) {

                            Toast.makeText(getContext(), "The \"read\" status is required to assign the rate.", Toast.LENGTH_SHORT).show();
                        }

                        else if (addBookTitleValue.getText().toString().isEmpty() ||

                                addBookAuthorValue.getText().toString().isEmpty()){
                            Toast.makeText(getContext(), "Required: title, author and status",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else {
                            book = new Book(addBookTitleValue.getText().toString(),
                                    addBookAuthorValue.getText().toString(),
                                    addBookStatusValue.getSelectedItem().toString(),
                                    (int) ratingBar.getRating());
                            BookViewModel.bookList.add(book);
                            Navigation.findNavController(v).navigate(
                                    R.id.action_bookFragment_to_bookListFragment);
                        }

            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ratingBar.getRating() >= 1 &&
                        (book.getStatus().equals("Want to read") ||
                                book.getStatus().equals("Reading"))) {

                    Toast.makeText(getContext(), "The \"read\" status is required to assign the rate.", Toast.LENGTH_SHORT).show();
                }

                else if (addBookTitleValue.getText().toString().isEmpty() ||

                        addBookAuthorValue.getText().toString().isEmpty()){
                    Toast.makeText(getContext(), "Required: title, author and status",
                            Toast.LENGTH_SHORT).show();
                }
                else {
                    Navigation.findNavController(v).navigate(
                            R.id.action_bookFragment_to_bookListFragment);
                }
            }
        });
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        addBookTitleValue.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                book.setTitle(addBookTitleValue.getText().toString());
                return false;
            }
        });

        addBookAuthorValue.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                book.setAuthor(addBookAuthorValue.getText().toString());
                return false;
            }
        });

        addBookStatusValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                book.setStatus( addBookStatusValue.getSelectedItem().toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                book.setRate((int) ratingBar.getRating());
            }
        });
    }
}
